from random import randint


player = input("Welcome! What is your name? ")

# guess loop
for guess in range(1,6):
    month = randint(1, 12)
    year = randint(1923, 2005)
    print(f"Guess {guess}: {player} were you born on {month} / {year} ?")
    response = input("yes or no: ").lower()
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess == 5:
        print("I have better things to do. Goodbye.")
    else:
        print("Drat! Let's try again.")
